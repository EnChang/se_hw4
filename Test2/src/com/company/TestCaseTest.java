package com.company;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestCaseTest {
    private letterGrade letterGrade;

    @BeforeEach
    void setUp() {
        letterGrade = new letterGrade();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    public void partitionValueTest() {
        Assertions.assertEquals('X', letterGrade.letterGrade(-1));
    }

    @Test
    public void partitionValueXTest() {
        Assertions.assertEquals('X', letterGrade.letterGrade(101));
    }

    @Test
    public  void partitionValueATest(){
        Assertions.assertEquals('A', letterGrade.letterGrade(95));
    }

    @Test
    public  void partitionValueBTest(){
        Assertions.assertEquals('B', letterGrade.letterGrade(85));
    }

    @Test
    public  void partitionValueCTest(){
        Assertions.assertEquals('C', letterGrade.letterGrade(75));
    }

    @Test
    public  void partitionValueDTest(){
        Assertions.assertEquals('D', letterGrade.letterGrade(65));
    }

    @Test
    public  void partitionValueFTest(){
        Assertions.assertEquals('F', letterGrade.letterGrade(59));
    }
}