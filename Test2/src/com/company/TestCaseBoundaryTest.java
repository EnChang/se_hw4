package com.company;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestCaseBoundaryTest {
    private letterGrade letterGrade;

    @BeforeEach
    void setUp() {
        letterGrade = new letterGrade();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    public void boundaryValueTest() {
        Assertions.assertEquals('X', letterGrade.letterGrade(-1));
    }

    @Test
    public void MaxBoundaryValueTest() {
        Assertions.assertEquals('X', letterGrade.letterGrade(101));
    }

    @Test
    public void MaxBoundaryValueATest() {
        Assertions.assertEquals('A', letterGrade.letterGrade(100));
    }

    @Test
    public void boundaryValueATest() {
        Assertions.assertEquals('A', letterGrade.letterGrade(90));
    }

    @Test
    public void MaxBoundaryValueBTest() {
        Assertions.assertEquals('B', letterGrade.letterGrade(89));
    }

    @Test
    public void boundaryValueBTest() {
        Assertions.assertEquals('B', letterGrade.letterGrade(80));
    }

    @Test
    public void MaxBoundaryValueCTest() {
        Assertions.assertEquals('C', letterGrade.letterGrade(79));
    }

    @Test
    public void boundaryValueCTest() {
        Assertions.assertEquals('C', letterGrade.letterGrade(70));
    }

    @Test
    public void MaxBoundaryValueDTest() {
        Assertions.assertEquals('D', letterGrade.letterGrade(69));
    }

    @Test
    public void boundaryValueDTest() {
        Assertions.assertEquals('D', letterGrade.letterGrade(60));
    }

    @Test
    public void MaxBoundaryValueFTest() {
        Assertions.assertEquals('F', letterGrade.letterGrade(59));
    }

    @Test
    public void boundaryValueFTest() {
        Assertions.assertEquals('F', letterGrade.letterGrade(0));
    }
}